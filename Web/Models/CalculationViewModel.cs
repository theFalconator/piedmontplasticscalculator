﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace Web.Models
{
    public class CalculationViewModel
    {

        [Required]
        public int X { get; set; }

        [Required]
        public int Y { get; set; }

        /// <summary>
        /// operation chosen by user
        /// </summary>
        [Required]
        public string Operation { get; set; }

        /// <summary>
        /// List of operations to populate dropdown in view
        /// </summary>
        public List<string> AllowedOperations { get; set; }

    }
}