﻿using Core.Interfaces;
using System.Web.Mvc;
using Web.Models;

namespace Web.Controllers
{
    public class HomeController : Controller
    {
        private readonly ICalculatorService calculatorService;

        public HomeController(ICalculatorService calculatorService)
        {
            this.calculatorService = calculatorService;
        }

        public ActionResult Index()
        {
            var vm = new CalculationViewModel
            {
                AllowedOperations = calculatorService.ListOperations()
            };

            return View(vm);
        }

        public ActionResult Calculate(CalculationViewModel vm)
        {
            if (!ModelState.IsValid)
            {
                Response.StatusCode = 400;
                vm.AllowedOperations = calculatorService.ListOperations();
                return View(nameof(Index), vm);
            }

            return Json(calculatorService.Calculate(vm.Operation, vm.X, vm.Y), JsonRequestBehavior.AllowGet);
        }
    }
}