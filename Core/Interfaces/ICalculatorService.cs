﻿using System.Collections.Generic;

namespace Core.Interfaces
{
    public interface ICalculatorService
    {
        List<string> ListOperations();
        int Calculate(string operation, int x, int y);
    }
}
