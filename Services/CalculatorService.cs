﻿using Core.Interfaces;
using System.Collections.Generic;

namespace Services
{
    public class CalculatorService : ICalculatorService
    {
        public int Calculate(string operation, int x, int y)
        {
            switch(operation)
            {
                case "+":
                    return x + y;
                case "-":
                    return x - y;
                default:
                    return 0;
            }
        }

        public List<string> ListOperations()
        {
            return new List<string> { "+", "-" };
        }
    }
}
