﻿using System.Linq;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Services;

namespace Tests
{
    [TestClass]
    public class CalculatorServiceShould
    {
        [TestMethod]
        public void ListAllAvailableOperations()
        {
            var sut = new CalculatorService();

            var operations = sut.ListOperations();

            // don't care about the order, just want one of the elements in the list to be '+' and one to be '-'
            Assert.IsTrue(operations.Any(o => o == "+"));
            Assert.IsTrue(operations.Any(o => o == "-"));
        }

        [DataTestMethod]
        [DataRow(1, 1)]
        [DataRow(10, 18)]
        [DataRow(1000, 1000000)]
        [DataRow(125, 13245987)]
        public void AddTwoPositiveNumbers(int x, int y)
        {
            var sut = new CalculatorService();

            var result = sut.Calculate("+", x, y);

            Assert.AreEqual(x + y, result);
        }

        [DataTestMethod]
        [DataRow(2, 1)]
        [DataRow(100, 18)]
        [DataRow(976522, 11235)]
        [DataRow(1, 100)]
        public void SubtractTwoPositiveNumbers(int x, int y)
        {
            var sut = new CalculatorService();

            var result = sut.Calculate("-", x, y);

            Assert.AreEqual(x - y, result);
        }

        [DataTestMethod]
        [DataRow(-1, -1)]
        [DataRow(-10, -18)]
        [DataRow(-1000, -113249786)]
        [DataRow(-125, -132)]
        public void AddTwoNegativeNumberse(int x, int y)
        {
            var sut = new CalculatorService();

            var result = sut.Calculate("+", x, y);

            Assert.AreEqual(x + y, result);
        }

        [DataTestMethod]
        [DataRow(-2, -1)]
        [DataRow(-100, -18)]
        [DataRow(-976522, -11235)]
        [DataRow(-1, -100)]
        public void SubtractTwoNegativeNumbers(int x, int y)
        {
            var sut = new CalculatorService();

            var result = sut.Calculate("-", x, y);

            Assert.AreEqual(x - y, result);
        }

        [TestMethod]
        public void AddInt32MinAndMaxValues()
        {
            var sut = new CalculatorService();

            var result = sut.Calculate("+", int.MinValue, int.MaxValue);
            Assert.AreEqual(-1, result);
        }

        [TestMethod]
        public void SubtractInt32MinAndMaxValues()
        {
            var sut = new CalculatorService();

            var result = sut.Calculate("-", int.MaxValue, int.MinValue);
            Assert.AreEqual(-1, result);
        }
    }


}
